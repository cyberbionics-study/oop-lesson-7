some_list_1 = []
some_list_2 = []
res_1 = []
res_2 = []
for x in range(10):
    some_list_1.append(x)

for x in range(10, 20):
    some_list_2.append(x)

for num in some_list_1:
    if num % 2 == 0:
        res_1.append(num ** 2)

print(res_1)

res_2 = [num ** 2 for num in some_list_2 if num % 2 == 0]

print(res_2)
